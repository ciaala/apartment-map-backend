'use strict';
import request from 'request-promise-native';

const headers =
    {
      'User-Agent':
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3968.0 Safari/537.36',
    }
;
/**
 *
 * @param {string} url
 * @returns {Promise<string>}
 */
export default function getHtml(url) {
  const options = {
    uri: url,
    headers,
  };
  return request(options);
}
