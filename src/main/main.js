'use strict';
import Init from './Init.js';
import Logger from './util/Logger.js';

class Application {
  constructor(port = 3000) {
    this.logger = new Logger();
    this.init = new Init();
  }

  systemInit() {
    this.logger.info('Starting up the apartment-map-backend');
    this.init.startup(
    ).then(function() {
        }.bind(this)
    ).catch(function(error) {
          this.logger.error('Error in startup');
          this.logger.error(error);
        }.bind(this)
    );
  }

  systemShutdown() {
    this.logger('[SHUTDOWN]');
    this.init.shutdown(
    ).then(function() {
          this.logger.info('Completed Shutdown');
        }.bind(this)
    ).catch(function(error) {
          this.logger.error(error);
        }.bind(this)
    );
  }

  applicationInit() {

    this.dataFacade.loadRawData().then(
        function(datas) {
          if (datas) {
            this.scope.areDataReady = true;
            this.logger.info(
                `Data are Ready. Loaded ${datas.length} Data Sources`);
          } else {
            this.logger.warn(`Failed to load data for the Data Sources`);
          }
        }.bind(this)).catch(function(error) {
      this.logger.error('Error loading the initial data');
      this.logger.error(error);
    }.bind(this));
  }

}

const
    application = new Application(3000);
application.systemInit();
