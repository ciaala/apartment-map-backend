'use strict';
import Logger from '../../util/Logger.js';
import jsdom from 'jsdom';

const JSDOM = jsdom.JSDOM;

export class HomegateDataExtractor {

  constructor(htmlCache) {
    this.logger = Logger();
    this.htmlCache = htmlCache;
  }

  /**
   *
   * @return Promise<NxPropertyData>
   */
  extract(uri) {
    this.logger.info('Loading data: ' + uri);
    const url = 'https://www.homegate.ch' + uri;

    return new Promise((resolve, fail) => {
      this.htmlCache.getCachedHtml(url).then(function(data) {
        const property = this.htmlToNx(uri, new JSDOM(data).window.document,
            data);
        resolve(property);
      }.bind(this)).catch(function(error) {
        fail(error);
      });
    });
  }

  /**
   *
   * @param {string} uri
   * @param {Document} document
   * @param {string} data
   * @return NxPropertyData
   */
  htmlToNx(uri, document, data) {
    this.logger.debug('Extracting data: ' + uri);

    const propertyMap = {uri};
    const tags = document.querySelectorAll('dt');

    tags.forEach(e => {
      const key = e.textContent.substr(0, e.textContent.length - 1);
      propertyMap[key] = e.nextElementSibling.textContent.trim();
    });
    const iframeGoogleMap = document.querySelector(
        'iframe.AddressDetails_map_f8Eb8');
    if (iframeGoogleMap) {
      const matches = iframeGoogleMap.src.match('q=([0-9.]+),([0-9.]+)');
      propertyMap['latitude'] = matches[1];
      propertyMap['longitude'] = matches[2];
    }
    for (const p in propertyMap) {
      if (propertyMap[p].startsWith('CHF')) {
        const sumString = propertyMap[p];
        const stripped = sumString.replace('CHF', '').trim();
        const thousandClean = stripped.replace(',', '');
        propertyMap[p] = parseInt(thousandClean);
      }
    }
    const pictures = {};
    //document.querySelectorAll('li.glide__slide img').forEach(v => this.logger.info(v.srcset));
    const pictureTags = document.querySelectorAll('li.glide__slide img');
    const searchString = 'attachments":';
    const attachmentsIndexOf = data.indexOf(searchString) + searchString.length;
    const closingIndexOf = data.indexOf(']', attachmentsIndexOf) + 1;
    if (attachmentsIndexOf === -1) {
      this.logger.error(
          'Unable to extract the picture for the property: ' + uri);
      propertyMap.pictures = {};
    } else {
      const text = data.substr(attachmentsIndexOf,
          closingIndexOf - attachmentsIndexOf);
      const pictures = JSON.parse(text);
      propertyMap.pictures = pictures.map(img => img.url);
    }
    /*
    for(let i=0; i < pictureTags.length; i++) {

      const picture = pictureTags[i];
      pictures[picture.src] = picture.srcset;
    }
    */

    const address = document.querySelector(
        'address.AddressDetails_address_3Uq1m').textContent;
    propertyMap['address'] = address;

    propertyMap['url'] = 'https://homegate.ch' + propertyMap['uri'];
    this.renameProperties(propertyMap);
    this.logger.info(propertyMap);
    return propertyMap;
  }

  /**
   *
   * @param propertyMap
   * @return NxPropertyData
   */
  renameProperties(propertyMap) {
    // const property = new NxPropertyData();
    const transformationMap = {
      'Floor': 'floor',
      'Rent': 'price',
      'No. of rooms': 'rooms',
      'Surface living': 'surface',
      'Type' : 'type',
      'Add\'l expenses': 'extra price',
      'Net rent': 'base price',
      'Year Built': 'year built',
      "Room height" : 'room height',
      "Available from": "available since"
    };
    for (const key in transformationMap) {
      propertyMap[transformationMap[key]] = propertyMap[key];
      delete propertyMap[key];
    }
    return propertyMap;
  }
}
