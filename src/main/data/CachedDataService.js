'use strict';

import AbstractDataSource from './AbstractDataSource.js';

export default class CachedDataService extends AbstractDataSource {

  /**
   *
   * @param {CacheServiceInterface} cacheService
   * @param {AbstractDataSource} dataSource
   * @param {boolean} onlyCache
   * @param {boolean} alwaysUseService
   */
  constructor(
      cacheService,
      dataSource,
      {onlyCache = false, alwaysUseService = false} = {}) {

    super(`Cached ${dataSource.getName()} (${cacheService.getName()})`);

    this.cacheService = cacheService;
    this.dataSource = dataSource;

    this.onlyCache = onlyCache;
    this.alwaysUseService = alwaysUseService;
  }

  load() {
    // TODO load the keys known in REDIS already
    return this.dataSource.load();
  }
}


