
export default class CacheServiceInterface {

  /**
   *
   * @param name
   */
  constructor(name) {
    this.name = name;
  }

  /**
   *
   * @return {String}
   */
  getName() {
    return this.name;
  }

  /**
   *
   * @param {string} key
   * @return {Promise<string>}
   */
  get(key) {
    throw new Error('Not implemented');
  }

  /**
   *
   * @param {string} key
   * @param {string} value
   */
  put(key, value) {
    throw new Error('Not implemented');
  }

}
