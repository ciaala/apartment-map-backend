'use strict';
import Logger from '../../util/Logger.js';
import Redis from 'ioredis';
import CacheServiceInterface from '../CacheServiceInterface.js';

export default class RedisCache extends CacheServiceInterface {
  /**
   *
   * @param {boolean=false} onlyCache should use the real service or just service from cache
   * @param {boolean=false} alwaysUseService
   */
  constructor({onlyCache = false, alwaysUseService = false} = {}, services) {
    super('RedisCache');
    this.alwaysUseService = alwaysUseService;
    this.onlyCache = onlyCache;
    this.redis = new Redis();
    this.logger = Logger();
    this.isCacheInitialised = false;

    const builtinCommands = this.redis.getBuiltinCommands();
    if (this.logger.isDebugEnabled()) {
      this.printRedisCommands(builtinCommands);
    }
  }

  static getDependencies() {
    return [];
  }

  init() {
    return new Promise((resolve, reject) => resolve());
  }

  printRedisCommands(builtinCommands) {
    const tempMap = {};
    builtinCommands.forEach(
        function(value) {
          let valueElement = value[0];
          tempMap[valueElement]
              ?
              tempMap[valueElement].push(value) :
              tempMap[valueElement] = [value];
        });
    this.redisCommands = tempMap;
    for (const letter in this.redisCommands) {
      this.logger.info(letter, this.redisCommands[letter].join(' '));

    }
  }

// -------------------------------------------------
  // LocalCacheInterface
  // -------------------------------------------------

  // Override
  /**
   *
   * @param {string} key
   * @returns {Promise<string>}
   */
  get(key) {
    return this.redis.get(key);
  }

// Override
  put(key, value) {
    return this.redis.set(key, value);
  }

// End of LocalCacheInterface
// -------------------------------------

// --------------------------------------------------
  shouldUseService() {
    return !this.onlyCache &&
        (this.alwaysUseService || !this.isCacheInitialised);
  }

  //
  // put(key, value) {
  //   this.logger.info(key, value);
  //   const promise = this.redis.set(key, value);
  //   promise.then(() => {
  //     this.logger.info('Stored:', {key, value});
  //   }).catch(function(error) {
  //     this.logger.error('Error persisting the key: ' + key, error);
  //   });
  // }

  /**
   *
   * @param {http.ServerResponse} response
   */
  getKeySet(response, filterPredicate) {
    let keys = [];
    const stream = this.redis.scanStream();
    stream.on('data', function(resultKeys) {
      const filtered = resultKeys.filter(v => filterPredicate(v));
      keys = keys.concat(filtered);
      this.logger.info('accumulating ' + filtered.length);
    }.bind(this));
    stream.on('end', () => this.onCacheKey(response, keys));

  }

  /**
   *
   * @param {()(string[])} keysConsumer
   * @returns {Promise<>}
   */
  getKeys(keysConsumer) {
    return new Promise(function(resolve, reject) {
      const stream = this.redis.scanStream();
      stream.on('data', keysConsumer);
      stream.on('end', () => resolve());
    }.bind(this));
  }

  /**
   *
   * @param {http.ServerResponse} response
   * @param {Array<String>} resultKeys
   */
  onCacheKey(response, resultKeys) {
    // `resultKeys` is an array of strings representing key names.
    // Note that resultKeys may contain 0 keys, and that it will sometimes
    // contain duplicates due to SCAN's implementation in Redis.

    const operations = [];

    for (let index = 0; index < resultKeys.length; index++) {
      const resultKey = resultKeys[index];
      this.logger.info('redis-store: ', resultKey);
      const op = this.redis.get(resultKey);
      operations.push(op);
    }

    // resultKeys
    //operations.push();
    //const op = redis.getAll(resultKeys)
    this.logger.info('Operations ' + operations.length);
    Promise.all(operations).
        then(function(values) {
          response.weSetHeader = true;
          response.setHeader('Content-Type', 'application/json');

          response.write('[');
          this.logger.info(`number of values: '${values.length}'`);
          for (let index = 0; index < values.length; index++) {
            const resultKey = resultKeys[index];
            const value = values[index];
            const entry = {
                  idx: index,
                  key: resultKey,
                  value: JSON.parse(value),
                  // size: value.length
                }
            ;
            this.logger.info('entry -- ', entry.key);
            response.write(JSON.stringify(entry));
            if ( index + 1 < values.length) {
              response.write(',');
            }
          }
          response.write(']');
          response.end();
        }.bind(this)).
        catch(function(error) {
              this.logger.error(error);
              response.status(500);
            }.bind(this)
        );
    //
  }
}
