//import google from "@google/maps";
import GoogleMapCachedDirectionService from './GoogleMapCachedDirectionService.js';

export default class GoogleMapHelper {

  isDebugMode = false;
  gmap;
  isGoogleMapLoaded = false;
  isMapDivInserted = true;
  markerCount = 0;
  markers = [];
  directionsService;
  directionDisplay;
  markerCache = new Set();
  CANOO_MARKER = {
    label: 'Canoo',
    latitude: 47.361451,
    longitude: 8.564295,
    color: '#CCF'
  };
  HOME_MARKER = {
    label: 'Home',
    latitude: 47.357409,
    longitude: 8.526324,
    color: '#CCF'
  };
  BURKLIPLATZ_MARKER = {
    label: 'Bürkliplatz',
    latitude: 47.366798,
    longitude: 8.541311
  };
  constructor(eventManager) {
    this.directionCache = new GoogleMapCachedDirectionService();
    this.eventManager = eventManager;
    this.eventManager.subscribe(this.eventManager.events.googleMapScript,
        this.setupMap.bind(this));
    this.eventManager.subscribe(this.eventManager.events.mapReady,
        this.setupMap.bind(this));

  }

  init() {
    window.googleMap = this;
    let mapDiv = document.getElementById('myMap');
    if (mapDiv === undefined || mapDiv === null) {
      mapDiv = document.createElement('div');
      mapDiv.setAttribute('id', 'myMap');
      mapDiv.style.height = '600px';
      mapDiv.style.width = '1280px';
      mapDiv.style.padding = '8px';
      this.eventManager.publish(this.eventManager.events.appendMap, mapDiv);
    }
    let scriptBlock = document.getElementById('scriptBlock');
    if (scriptBlock === null || scriptBlock === undefined) {
      const YOUR_API_KEY = 'AIzaSyCvrW2I6KxWZSCWOWAxgSzOZ0nIXmJZDao';
      const srcString = `https://maps.googleapis.com/maps/api/js?key=${YOUR_API_KEY}&callback=window.googleMap.initMap`;

      scriptBlock = document.createElement('script');
      scriptBlock.setAttribute('id', 'scriptBlock');
      scriptBlock.setAttribute('src', srcString);
      scriptBlock.setAttribute('defer', 'true');
      scriptBlock.setAttribute('async', 'true');

      document.body.append(scriptBlock);
    }

  }
  getDateToNextMonday() {
    const MONDAY = 1;
    const now = new Date();
    let time;
    if (now.getDay() !==  MONDAY) {
      const nextMondayOn = (now.getDay() > MONDAY) ? (1 + 7) : 1;
      const increment =  nextMondayOn - now.getDay();
      time = new Date( now.getTime() + (increment * 1000* 60*60*24));
    } else {
      time = now;
    }
    time.setHours(8, 30);
    return time;
  }

  calculateDirection(marker) {
    const time = this.getDateToNextMonday();

    const homeMarker = this.HOME_MARKER;
    const path = {
      origin: marker.position,
      destination: {
        lat: homeMarker.latitude,
        lng: homeMarker.longitude
      },
      travelMode: 'TRANSIT',
      transitOptions: {
        departureTime: time
      }
    };

    return new Promise(
        function(resolveDirection, fail) {
          this.directionCache.get(path)
          .then(
            function(response){
          if ( response ) {
            console.log('Cached Directions Response');
            new Promise( function(resolve, fail) {
              this.directionDisplay.setDirections(response);
                resolve();
              });
          } else {
              this.directionsService.route(path,
              function(response, status) {
                if (status === 'OK') {
                  this.directionDisplay.setDirections(response);
                  this.directionCache.set(path, response);
                } else {
                  console.error(
                      `Directions request from ${marker.label.textContent} to ${homeMarker.label} at ${time} has failed. Error ${status}`);
                }
              });
          }
        }.bind(this)
      );
    });
  }

  pinSymbol(color) {
    return {
      path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
      fillColor: color,
      fillOpacity: 1,
      strokeColor: '#444',
      strokeWeight: 1,
      labelOrigin: new this.google.maps.Point(0, -28),
      scale: 0.8
    };
  }

  markerClickListener(markerData, marker) {
    if (markerData.hasOwnProperty('infoPopup')) {
      this.calculateDirection(marker);
      const infowindow = new this.google.maps.InfoWindow({
        content: markerData.infoPopup
      });
      infowindow.open(this.gmap, marker);
    } else {
      const value = markerData.label.toString();
      console.log(value);
      if ('Ηοme' !== value) {
        window.open(markerData.url, '_blank');
      }
    }
  }

  addMarker(eventName, markerData) {
    const latLang = new this.google.maps.LatLng(
        markerData.latitude,
        markerData.longitude);
    let label;
    if (markerData.label) {
      label = markerData.label;
    }

    const marker = new this.google.maps.Marker({
      position: latLang,
      title: markerData.title,
      label: {
        text: label,
        fontSize: '12px',
        color: markerData.labelColor,
        textShadow: '2 2 grey'
      },
      url: markerData.url,
      visible: true,
      map: this.gmap,
      icon: this.pinSymbol(markerData.color)
    });
    marker.setMap(this.gmap);
    marker.addListener('hover',
        () => this.markerClickListener(markerData, marker));

    const markerIndex = this.markers.push(marker);
    let markerId;
    if (markerData.url !== undefined && markerData.url !== null) {
      const markerTokens = markerData.url.split('/');
      markerId = markerTokens[markerTokens.length - 2];
    } else {
      markerId = 'Home';
    }
    if (this.isDebugMode) {
      console.log({
        action: 'register maker',
        markerIndex,
        markerId,
        label: markerData.label
        /*
          markerLatitude: markerData.latitude,
         markerLongitude: markerData.longitude,*/
      }
      );
      if (this.markerCache.has(markerId) === true) {
        console.log('already seen marker ' + markerId);
        console.log(markerData);
      } else {
        this.markerCache.add(markerId);
      }
    }
  }

  clearMap(eventName, eventData) {
    if (this.gmap !== null && this.gmap !== undefined) {
      for (let i = 1; i < this.markers.length; i++) {
        this.markers[i].setMap(null);
      }
      this.markers.length = 1;
    }
    this.markerCache.clear();
  }

  initMap() {
    console.log('InitMap has been triggered');
    this.google = window.google;

    this.eventManager.publish(this.eventManager.events.googleMapScript);
  }

  setupMap(eventName, eventData) {
    const google = this.google;
    if (eventName === this.eventManager.events.mapReady) {
      this.isMapDivInserted = true;
    }
    if (eventName === this.eventManager.events.googleMapScript) {
      this.isGoogleMapLoaded = true;
    }

    if (this.isMapDivInserted === true && this.isGoogleMapLoaded === true) {

      const mapDiv = document.getElementById('myMap');
      this.gmap = new google.maps.Map(mapDiv, {
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: {lat: this.BURKLIPLATZ_MARKER.latitude, lng: this.BURKLIPLATZ_MARKER.longitude}
      });
      //center: {
      //           lat: 47.361451,
      //           lng: 8.564295
      //         }
      this.directionsService = new google.maps.DirectionsService();
      this.directionDisplay = new google.maps.DirectionsRenderer(
          {preserveViewport: true});
      this.directionDisplay.setMap(this.gmap);
      this.directionDisplay.setPanel(document.getElementById('mapsRightPanel'));
      this.eventManager.publish(this.eventManager.events.addMarker,
          this.HOME_MARKER
      );
      this.eventManager.subscribe(this.eventManager.events.addMarker,
          this.addMarker.bind(this));
      this.eventManager.subscribe(this.eventManager.events.clearMap,
          this.clearMap.bind(this));
      this.eventManager.subscribe(this.eventManager.events.centerMap,
          this.centerMap.bind(this));
      this.eventManager.publish(this.eventManager.events.registerButton,
          {label: 'Clear Map', eventName: 'clearMap'});
    }
  }

  centerMap(eventName, eventData) {
    this.gmap.setZoom(12);
    this.gmap.setCenter(this.markers[0].position);
  }
}
