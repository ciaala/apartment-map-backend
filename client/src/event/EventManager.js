
function EventManager() {
  "use strict";
  this.events = {
    registerToolbar: 'registerToolbar',
    appendMap: 'appendMap',
    googleMapScript: 'googleMapScript',
    mapReady: 'mapReady',
    addMarker: 'addMarker',
    registerButton: 'registerButton',
    loadData: 'loadData',
    centerMap: 'centerMap',
    clearMap: 'clearMap',
  };
  let subscribers = {};
  let eventQueue = {};
  let eventHistory = [];
  let isDebugMode = false;

  function fireEvent(eventName, eventData) {
    if (subscribers[eventName] !== null && subscribers[eventName] !== undefined) {
      for (let subscriber of subscribers[eventName]) {
        subscriber(eventName, eventData);
        if (isDebugMode === true) {
          eventHistory.push({
            eventName: eventName,
            eventData: eventData,
            subscriber: subscriber,
            valueOf: function () {
              return this.eventName;
            }
            //caller: arguments.callee.caller.name
          });
        }
      }
      return true;
    } else {
      return false;
    }
  }

  this.publish = function (eventName, eventData) {
    if (isDebugMode === true) {
      console.log('Fired ' + eventName);
    }
    if (!fireEvent(eventName, eventData)) {
      if (isDebugMode === true) {
        console.log('No event handler registered: "' + eventName + '"');
      }
      if (eventQueue[eventName] !== null && eventQueue[eventName] !== undefined) {
        eventQueue[eventName].push(eventData);
      } else {
        eventQueue[eventName] = [eventData];
      }
    }
  };

  this.subscribe = function (eventName, subscriber) {
    if (subscribers[eventName] === null || subscribers[eventName] === undefined) {
      subscribers[eventName] = [subscriber];
    } else {
      subscribers[eventName].push(subscriber);
    }
    if (isDebugMode === true) {
      console.log('Registered subscriber "' + eventName + '"');
    }
    if (eventQueue[eventName] !== null && eventQueue[eventName] !== undefined) {
      console.log('Firing queued event[' + eventQueue[eventName].length + ']');
      for (let eventData of eventQueue[eventName]) {
        fireEvent(eventName, eventData);
      }
      eventQueue[eventName] = null;
    }
  };

  this.dumpEventHistory = function () {
    return eventHistory;
  };
}
module.exports = EventManager;
