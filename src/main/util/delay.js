'use_strict';
export default function __delay__(timer) {
  return new Promise(resolve => {
    timer = timer || 2000;
    setTimeout(function () {
      resolve();
    }, timer);
  });
}
