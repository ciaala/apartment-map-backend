'use strict';
import pino from 'pino';
import pinoPretty from 'pino-pretty';
import pinoCaller from 'pino-caller';

export default /**
 *
 * @returns {pino}
 * @constructor
 */
function Logger(service = 'none') {
  const options = true ? {
    prettyPrint: {
      levelFirst: true,
      // translateTime: true,
      ignore: 'pid,hostname,time',

    },
    prettifier: pinoPretty,
  } : {} ;
  const result = pino(options);
  const callerPrinter = pinoCaller(result);

  result.isDebugEnabled = function() {
    this.isLevelEnabled('debug');
  }.bind(result);

  return callerPrinter;
}

