
/**
 * Interface to expose the method consume()
 */
export default class ConsumerInterface {

  /**
   * Model a Map entry made of uri and value
   *
   * @typedef {Object} nx.UriDataEntry
   * @property {String} uri - the uri as a key
   * @property {String} value - the value / data belonging to the Uri
   */

  /**
   * @define
   * @param {nx.UriDataEntry} data
   */
  consume(uriDataEntry);
}
