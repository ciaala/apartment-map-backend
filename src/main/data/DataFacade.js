'use strict';

import Logger from '../util/Logger.js';
import Homegate from './sources/Homegate.js';

/**
 *  Facade to access the Data
 */
export default class DataFacade {
  constructor(conf= {}, services) {
    this.logger = Logger();
    this.homegate = services.get('Homegate');

    this.sources = [this.homegate];
  }

  /**
   * Promise
   */
  init() {
    return this.loadRawData();
  }
  static getDependencies() {
    return [Homegate];
  }

  /**
   *
   * @returns {Promise<>}
   */
  loadRawData() {
    return new Promise((resolve, fail) => {resolve();});
    // const promises = [];
    // const numberOfSources = this.sources.length;
    // for (let i = 0; i < numberOfSources; i++) {
    //   const sourceElement = this.sources[i];
    //   this.logger.info(`[LOAD DataSource ${i +
    //   1}/${numberOfSources}] <${sourceElement.getName()}>`);
    //   promises.push(sourceElement.load());
    //
    // }
    // return Promise.all(promises).
    //     then(function(values) {
    //       this.logger.info('[DONE]');
    //     }.bind(this)).
    //     catch(function(error) {
    //       this.logger.error(error);
    //     }.bind(this));
  }

  getRawData(response) {
    return this.homegate.getKeySet(response,
        (uri) => uri.startsWith('/rent/')
    );
  }
}
