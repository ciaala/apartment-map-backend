function copyProperties(target, source, properties) {
  for (let property of properties) {
    target[property] = source[property];
  }
  return target;
}

function makeGenericLabelColor(backgroundColor) {
  let a = 1 -
      (0.299 * backgroundColor.red + 0.587 * backgroundColor.green + 0.114 *
          backgroundColor.blue) / 255;
  let labelColor;
  if (a < 0.5) {
    labelColor = 'darkslategrey'; // bright colors - black font
  } else {
    labelColor = 'honeydew'; // dark colors - white font
  }
  return labelColor;
}

function makeLabelColor(backgroundColor) {
  if (backgroundColor.red > 128) {
    return 'seagreen';
  } else if (backgroundColor.green > 128) {
    return 'darkslategrey';
  } else if (backgroundColor.blue > 128) {
    return 'orange';
  } else {
    return 'white';
  }
}

function makeColor(data) {
  return makeColorByPrice(data.price, 1500, 3000);
}

function makeLabel(data) {
  return data.rooms + '/' + data.surface;
}

function rgb2hex(red, green, blue) {
  const rgb = blue + (green * 256) + (red * 65536);
  return '#' + (0x1000000 + rgb).toString(16).slice(1);
}

function makeColorByPrice(price, min_price, max_price) {
  let green, blue, red;
  green = blue = red = 32;
  if ((price === undefined || price === null) || (min_price >= max_price)) {
    return {red, green, blue};
  }
  if (price > max_price) {
    price = max_price;
  }
  const position = (price - min_price) / (max_price - min_price);
  if (position < 0.34) {
    green = 255 - Math.ceil(3 * 128 * position);
  } else if (position < 0.67) {
    blue = 255 - Math.ceil(3 * 128 * (position - 0.33));
  } else {
    red = 255 - Math.ceil(3 * 128 * (position - 0.66));
  }
  return {red, green, blue};
}

export default class ApiClient {
  constructor(eventManager) {
    this.eventManager = eventManager;
  }

  loadData() {
    fetch('/data').
        then(function(response) {
          response.json().
              then(function(data) {
                for (let i = 0; i < data.length; i++) {
                  this.prepareMarker(data[i].value);
                }
              }.bind(this));
        }.bind(this)).
        catch(error => console.error(error));
  }

  prepareMarker(allData) {
    let backgroundColor = makeColor(allData);
    allData.infoPopup = document.createElement('ul');
    for (let key of [
      'price',
      'rooms',
      'surface',
      'available since',
      'floor',
      'address']) {
      const node = document.createElement('li');
      node.textContent = key + ': ' + allData[key].toString();
      allData.infoPopup.appendChild(node);
    }
    const link = document.createElement('a');
    link.textContent = 'Open';
    link.href = allData.url;
    link.addEventListener('click', function(event) {
      event.preventDefault();
      window.open(allData.url, '_blank');
    });
    allData.infoPopup.appendChild(link);
    allData.color = rgb2hex(backgroundColor.red, backgroundColor.green,
        backgroundColor.blue);
    allData.labelColor = makeLabelColor(backgroundColor);
    allData.label = makeLabel(allData);
    this.updateMap(allData);
  }

  updateMap(data) {
    if (data.latitude && data.longitude) {
      this.eventManager.publish(this.eventManager.events.addMarker,
          data);
    } else {
      console.log('error in update map ', data);
    }
  }

  addMapElement(eventName, mapElement) {
    /**
     //let titleElement = document.getElementById('listBreadcrumb');
     //let titleElement = document.getElementsByClassName('search-results-header')[0];
     let titleElement = document.getElementsByClassName('regio-star-ad')[0];
     let wrapper = document.createElement('div');
     wrapper.setAttribute('id', 'ffi-wrapper');
     wrapper.style.width = '100%';
     wrapper.style.height = '600px';
     wrapper.style.border = '1px solid blue';
     //wrapper.style.backgroundColor = '#808080';
     //wrapper.style.padding = '4px';
     titleElement.parentNode.insertBefore(wrapper, titleElement);
     titleElement.parentNode.removeChild(titleElement);
     let otherBar = document.getElementsByClassName(
     'search-results-actions')[0];
     otherBar.parentNode.removeChild(otherBar);

     let rightPanel = document.createElement('div');

     rightPanel.setAttribute('id', 'mapsRightPanel');
     mapElement.style.float = 'left';
     mapElement.style.width = '75%';

     wrapper.append(mapElement);
     wrapper.append(rightPanel);
     rightPanel.style.float = 'right';
     rightPanel.style.width = '25%';
     rightPanel.style.paddingLeft = '8px';
     document.styleSheets[0].addRule('#mapsRightPanel .adp-summary',
     'font-weight: bold');
     this.eventManager.publish(this.eventManager.events.mapReady, mapElement);
     */
  }

}
