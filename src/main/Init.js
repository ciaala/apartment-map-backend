import API from './api/API.js';
import DataFacade from './data/DataFacade.js';
import HttpRedisCache from './data/redis-cache/HttpRedisCache.js';
import RedisCache from './data/redis-cache/RedisCache.js';
import Homegate from './data/sources/Homegate.js';

import Logger from './util/Logger.js';

function getSecondsSinceEpoch() {
  return Date.now();
}

/**
 *
 * @param {string} serviceName
 * @param {Map<string,class>}runningServicesMap
 * @param {number} timeout
 */
function stillRunning(serviceName, runningServicesMap, timeout) {
  const startTime = getSecondsSinceEpoch();
  const logger = Logger();

  function internalStartTime(runningTimeout) {
    if (startTime !== 0 && !runningServicesMap.has(serviceName)) {
      const deltaSeconds = Math.floor(
          (getSecondsSinceEpoch() - startTime) / 1000);
      const message = `Service ${serviceName} is still not initialised after ${deltaSeconds} seconds`;
      if (deltaSeconds > 4 * (timeout / 1000)) {
        logger.error(message);
      } else {
        logger.warn(message);
      }
      setTimeout(() => internalStartTime(
          runningTimeout * 2),
          runningTimeout * 2);
    }
  }

  setTimeout(() => internalStartTime(timeout), timeout);
}

export default class Init {
  constructor(conf = {API: {port: 3000}}) {
    this.logger = Logger();
    this.conf = conf;
    /** @type {Set<class>} */
    this.serviceMap = new Map(
        [
          DataFacade,
          HttpRedisCache,
          API,
          Homegate,
          RedisCache
        ].map(service => [service.name, service])
    );
    /** @type {Set<string>} */
    this.startSet = new Set();

    this.runningServiceMap = new Map();
    this.dependenciesMap = this.createDependenciesMap(this.serviceMap);
  }

  /**
   *
   * @param {Map<string, class>}services
   * @returns {Map<string, string[]>}
   */
  createDependenciesMap(services) {
    const dependenciesMap = new Map();
    services.forEach(function(service) {
      try {
        const dependencies = service.getDependencies();
        dependenciesMap.set(service.name, dependencies.map(s => s.name));
        if (dependencies.length === 0) {
          this.startSet.add(service.name);
        }
      } catch (e) {
        this.logger.error(
            `Unable to load the dependencies of service ${service.name}`);
        throw e;
      }
    }.bind(this));
    return dependenciesMap;
  }

  canStartup() {
    const simpleExecutionList = this.makeSimpleExecutionList();
    return simpleExecutionList.length === this.serviceMap.size;
  }

  /**
   *
   * @returns {string[]}
   */
  makeSimpleExecutionList() {
    // const toStartSet = new Set(this.startSet);
    /** @type {Map<string,string[]>} */
    const liveDependencies = new Map();
    this.dependenciesMap.forEach((value, key) =>
        liveDependencies.set(key, JSON.parse(JSON.stringify(value))));
    /** @type {string[]} */
    const readySet = [];
    /** @type {string[]} */
    const collected = [];

    this.startSet.forEach(
        value => readySet.push(value) && liveDependencies.delete(value));

    while (readySet.length > 0) {
      const completed = readySet.pop();

      for (const service of liveDependencies.keys()) {
        /** @type string[] */
        const currentDependencies = liveDependencies.get(service);
        const index = currentDependencies.indexOf(completed);

        if (index !== -1) {
          currentDependencies.splice(index, 1);

          if (currentDependencies.length === 0) {
            readySet.push(service);
            liveDependencies.delete(service);
          } else {
            liveDependencies.set(service, currentDependencies);
          }
        }
      }
      collected.push(completed);
    }
    return collected;
  }

  /**
   *
   * @param {string[]} validSequence
   * @param {function(Object)} resolve
   * @param {function(Error)} fail
   * @param {number} index
   */
  startServiceInSequence(validSequence, resolve, fail, index = 0) {
    const serviceName = validSequence[index];
    const serviceType = this.serviceMap.get(serviceName);
    this.logger.info('[START] ' + serviceName);
    const service = new serviceType(this.conf[serviceName],
        this.runningServiceMap);
    stillRunning(serviceName, this.runningServiceMap, 4000);
    if (!service['init']) {
      throw new Error(
          `Invalid Service type ${serviceName}. The service is missing a method init`);
    }
    service.init().then(function(data) {
          this.logger.info('[DONE] ' + serviceName);
          this.runningServiceMap.set(serviceName, service);
          if (index + 1 < validSequence.length) {
            this.startServiceInSequence(validSequence, resolve, fail, index + 1);
          } else {
            this.logger.info('[SYSTEM READY]');
            resolve();
          }
        }.bind(this)
    ).catch(function(error) {
      this.logger.error(error);
    }.bind(this));
  }

  startup() {
    return new Promise(
        function(resolve, fail) {
          if (this.canStartup()) {
            this.logger.info('[INIT] Starting up sequence');
            const validSequence = this.makeSimpleExecutionList();
            validSequence.forEach(
                function(value, index) {
                  const dependencies = this.dependenciesMap[value];
                  const dependenciesString = dependencies ?
                      '<=' + dependencies.join(' #') :
                      '';
                  this.logger.info(`${index} ${value} ${dependenciesString}`);
                }.bind(this));
            this.logger.info('[SYSTEM START]');
            this.startServiceInSequence.call(this, validSequence, resolve,
                fail);

          } else {
            const serviceClone = [];
            this.serviceMap.forEach(v => serviceClone.push(v.name));
            const validSequence = this.makeSimpleExecutionList();
            validSequence.forEach(
                value => serviceClone.splice(serviceClone.indexOf(value), 1));

            const notStartableServices = Array.prototype.join.call(serviceClone,
                ', ');
            fail(new Error(
                'Startup sequence is not runnable. Cannot start the following services: ' +
                notStartableServices));

          }
        }.bind(this)
    );
  }

  shutdown() {

  }
}
