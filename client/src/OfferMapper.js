"use strict";

const EventManager = require('./event/EventManager.js');
const GoogleMapHelper = require('./gmap/GoogleMapHelper.js');
// const HomeGateAdapter = require( './src/provider/HomeGate.js');

export default class OfferMapper {
  websiteAdapter;

  constructor(eventManager) {
    this.eventManager = eventManager;
  }
//    eventManager.subscribe(eventManager.events.registerButton, {label: 'refresh', handler: refreshData});

   initializeDataMapper() {
   /* if (window.location.origin.indexOf('immoscout24.ch') !== -1) {
      websiteAdapter = new ImmoScoutAdapter(eventManager);
    } else if (window.location.origin.indexOf('homegate.ch')) {
      websiteAdapter = new HomeGateAdapter(eventManager);
    }*/
  }

   init() {
    if (window.googleMap !== undefined && window.googleMap !== null) {
      window.googleMap.reset();
    } else {
      window.googleMap = new GoogleMapHelper(this.eventManager);
    }
    // websiteAdapter = initializeDataMapper();
    this.eventManager.publish(this.eventManager.events.registerToolbar, {
      buttons: [
        {
          label: 'Load Data',
          eventName: this.eventManager.events.loadData
        },
        {
          label: 'Center Map',
          eventName: this.eventManager.events.centerMap
        },
      ]
//          eventManager.publish(eventManager.events.registerButton, buttonDefinition);

    });
  }

  getEventManager() {
    return this.eventManager;
  };
}
