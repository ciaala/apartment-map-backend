# 2019 November 24 - November 26

## Doing
Identify the relevant Redis commands that should be used to persist and read data
ioredis.set
ioredis.scan a streaming api


## To discover

### JSDOC
how to add default value to parameters
modularity : how to write and import typedef only files
 
 
### ES6
Should I use 'use strict'; in the new modules or is it implied ?
or how can I configure the entire source files to be implied as ES6

Promises.then, Promises.catch and binding this
