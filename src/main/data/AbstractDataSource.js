'use strict';
import Logger from '../util/Logger.js';

/**
 * @typedef {Object} NxPropertyData
 * @property {string} uuid
 * @property {number} price
 * @property {NxAddress} address
 * @property {Array<NxRoom>} rooms
 * @property {number} floor
 * @property {Array<string>} pictures
 */
/**
 * @typedef {Object} NxRoom
 * @property {number} width
 * @property {number} height
 */
/**
 * @typedef {Object} NxAddress
 * @property {string} street
 * @property {string} number
 * @property {string} Nation
 * @property {string} City
 * @property {string} PostalCode
 */


/**
 * @enum {number}
 */
const GroundFloor = {
  NOT_RELEVANT: 0,
  ONLY_GROUND_FLOOR: 1,
  NO_GROUND_FLOOR: 2,
};

export default class AbstractDataSource {
  constructor(name) {
    this.name = name;
    this.logger = Logger();
    this.logger.info('[DataSource ' + this.getName() + ']');
  }

  /**
   *
   * @return {string}
   */
  getName() {
    return this.name;
  }

  /**
   * @param {NxSearchingOptions} searchingOptions
   * @return {Promise<Array<NxPropertyData>>}
   */
  load() {
    throw new Error('Not Implemented ' + this.getName());
  }

}
