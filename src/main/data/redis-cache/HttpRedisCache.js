import __delay__ from '../../util/delay.js';
import Logger from '../../util/Logger.js';
import RedisCache from './RedisCache.js';
import getHTML from '../../net/GetHtml.js';

export default class HttpRedisCache {
  /**
   *
   * @param useOnlyDataInCache
   */
  constructor(useOnlyDataInCache = false) {
    this.logger = Logger();
    this.redisCache = new RedisCache({});
    this.allowGetHtml = !useOnlyDataInCache;
    this.knownKey = {};
    this.isCacheLoading = true;

  }
  init() {
    return this.redisCache.getKeys(
        function(keys) {
          this.logger.info('Loading keys ' + keys.length);
          for (const key of keys) {
            this.knownKey[key] = true;
          }
        }.bind(this)
    ).then(function() {
          this.isLoading = false;
        }.bind(this)
    ).catch(error => {
      this.isLoading = false;
      this.logger.error(error);
    });
  }

  static getDependencies() {
    return [];
  }

  getCachedHtml(uri) {

    if (this.knownKey.hasOwnProperty(uri)) {
      this.logger.info('---- USE CACHE ', {uri});
      return this.redisCache.get(uri);
    } else if (this.allowGetHtml) {
      this.logger.info('---- USE getHTML ', {uri});
      const dataPromise = getHTML(uri);
      // TODO maybe we can run the getHTML and asynchronously run the set
      return new Promise(function(resolve, fail) {
        dataPromise.then(function(data) {
          this.redisCache.put(uri, data);
          // TODO asynchronous ??
          // TODO resolve(result) vs resolve(data)
          this.knownKey[uri] = true;
          resolve(data);
        }.bind(this)).catch(error => fail(error));
      }.bind(this));
    } else {
      this.logger.error(
          `---- cannot retrieve the resource: ${uri}. It is not in the cache and networking is disabled`);
    }
  }

  isCacheLoading() {

    return new Promise(function(resolve, reject) {
          async function waitFor() {
            let time = 0;
            while (this.isLoading) {
              await __delay__(500);
              time += 500;
            }
            this.logger.info('Waited ' + time);
            resolve();
          }

          waitFor.apply(this);
        }.bind(this)
    );
  }
}
