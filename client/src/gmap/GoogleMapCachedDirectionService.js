'use strict';


export default class GoogleMapCachedDirectionService {

  constructor() {
    this.cache = new Map();
  }

  set(key, value) {
    return new Promise( function(resolve, fail) {
      resolve(this.cache.set(key, value));
    });
  }

  get(key) {
    return new Promise ( function(resolve, fail) {
      resolve(this.cache.get(key));
    });
  }
};
