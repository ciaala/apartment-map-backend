/**
 * @typedef {Object} NxRange
 * @property {number} from
 * @property {number} to
 */
export class NxRange {
  from;
  to;

  constructor(from, to) {
    this.from = from;
    this.to = to;
  }
}

/**
 * @typedef {Object} NxSearchOptions
 * @property {string} where
 * @property {number} radius
 * @property {NxRange} rooms
 * @property {NxRange} price
 * @property {NxRange} livingSpace
 * @property {NxRange} yearBuilt
 * @property {GroundFloor} requireGroundFloor
 * @property {boolean} hasBalcony
 * @property {boolean} hasElevator
 * @property {boolean} hasWheelChairAccess
 */
export class NxSearchOptions {
  where;
  radius;
  rooms;
  price;
  livingSpace;
  yearBuilt;
  requireGroundFloor;
  hasBalcony;
  hasElevator;
  hasWheelChairAccess;

  /**
   *
   */
  constructor(base) {
    for (const p in base) {
      if (this.hasOwnProperty(p)) {
        this.p = base.p;
      }
    }
  }
}

