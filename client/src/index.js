import ApiClient from './api/ApiClient.js';
import EventManager from './event/EventManager.js';
import GoogleMap from './gmap/GoogleMapHelper.js';
import OfferMapper from './OfferMapper.js';


const eventManager = new EventManager();
const googleMap = new GoogleMap(eventManager);
const offerMapper = new OfferMapper(eventManager);
const apiClient = new ApiClient(eventManager);
window.ffi = {eventManager, googleMap, offerMapper, apiClient};
console.log('Yes we can serve the page', window.ffi);
googleMap.init();
apiClient.loadData();
