import jsdom from 'jsdom';
import AbstractDataSource from '../AbstractDataSource.js';
import {NxRange} from '../model/NxModel.js';
import HttpRedisCache from '../redis-cache/HttpRedisCache.js';
import RedisCache from '../redis-cache/RedisCache.js';
import {HomegateDataExtractor} from './HomegateDataExtractor.js';

const JSDOM = jsdom.JSDOM;

export default class Homegate extends AbstractDataSource {
  RENT_LIMIT = '3000';
  POSTAL_AREA = '8001';
  RADIUS = '5000';

  constructor(_, serviceMap) {
    super('Homegate');
    this.htmlCache = serviceMap.get('HttpRedisCache');
    this.extractor = new HomegateDataExtractor(this.htmlCache);
    this.redisCache = serviceMap.get('RedisCache');
  }

  /**
   *
   * @returns {string[]}
   */
  static getDependencies() {
    return [HttpRedisCache, RedisCache];
  }

  /**
   * @return Promise<NxPropertyData[]>
   */
  init() {
    // const promises = [this.isLoading(), initialLoad];
    return new Promise(function(resolve, fail) {
      this.loadDataSource({}).
          then(() => resolve());
    }.bind(this)).
        catch(function(error) {
          this.logger.error(error);
        }.bind(this));
  }

  /**
   * @param {http.ServerResponse} response
   * @param {NxSearchOption} searchOptions
   */
  search(response, searchOptions) {
    throw new Error('Not Implemented');
  }

  /**
   * @param {NxSearchOption} searchOptions
   */
  loadDataSource(searchOptions) {
    return this.fetchPaginationData(searchOptions).then(
        function(range) {
          const promises = [];
          for (let pageIndex = range.from; pageIndex < range.to; pageIndex++) {
            this.logger.info('Loading page index: ' + pageIndex);
            const promise = this.extractListHomegateProperties(pageIndex).then(
                function(data) {
                  this.logger.error('Loaded Data Fucking finally');
                }).catch(function(error) {
              this.logger.error(error);
            }.bind(this));
            promises.push(promise);
          }
          return Promise.all(promises);
        }.bind(this)
    ).catch(function(error) {
      this.logger.error('Unable to load Properties for ' + this.getName());
      this.logger.error(error);
      throw error;
    }.bind(this));
  }

  /**
   * @return NxRange
   */
  extractPaginationIndexes(document) {
    this.logger.info('extractPaginationIndexes');
    let container = document.getElementsByClassName('paginator-container')[0];
    if (container !== null && container !== undefined) {
      let counter = container.getElementsByClassName('paginator-counter')[0];
      let originalUrl = container.getElementsByClassName(
          'paginator')[0].children[0].children[0].href;
      let matches = originalUrl.match(/^(.*&ep=)[1-9][0-9]*(&.*)?$/);
      if (matches === null) {
        matches = ['', originalUrl + '&ep=', ''];
      } else if (matches.length === 3 && matches[2] === undefined) {
        matches = ['', matches[1], ''];
      }
      if (counter !== null && counter !== undefined) {
        const ranges = counter.getElementsByTagName('span');
        const min = parseInt(ranges[0].textContent);
        const max = parseInt(ranges[1].textContent);
        if ((min <= max && min === 1)) {
          return new NxRange(min, max);
        } else {
          this.logger.error(
              `Error while extracting the pagination indexes. '${matches}'`);
        }
      }
    }
    return new NxRange(1, 1);
  }

  fetchPaginationData() {
    return new Promise(function(resolve, fail) {
      // TODO Build the queries

      const url = this.makeRemoteUrl();
      this.logger.debug('Loading data: ' + url);
      this.htmlCache.getCachedHtml(url).
          then(function(data) {
            const document = new JSDOM(data).window.document;
            const paginationIndexes = this.extractPaginationIndexes(document);
            this.fetchArticlesFromPages(paginationIndexes).
                then(function(data) {
                  this.logger.info('processed all the pages ');
                  resolve(data);
                }.bind(this)).catch(function(error) {
              this.logger.error(error);
            }.bind(this));
          }.bind(this)).
          catch(function(err) {
            this.logger.error('Error consuming: ' + url);
            this.logger.error(err);
            fail(err);
          }.bind(this));
    }.bind(this));
  }

  /**
   *
   * @param {NxRange} range
   * @returns {Promise<void>}
   */
  fetchArticlesFromPages(range) {
    const promises = [];
    for (let pageIndex = range.from; pageIndex <= range.to; pageIndex++) {
      const url = this.makeRemoteUrl(pageIndex);
      const promise = new Promise(function(resolve, fail) {
        this.htmlCache.getCachedHtml(url).
            then(function(data) {
              // TODO should we make a chain promise ?
              this.extractListHomegateProperties(url, data).
                  then(() => resolve()).
                  catch(function(error) {
                    this.logger.error(error);
                  }.bind(this));
            }.bind(this)).
            catch(function(error) {
              this.logger.error(
                  `Unable to load properties from page ${pageIndex} of query ${url}`);
              fail(error);
            }.bind(this));
      }.bind(this)).
          catch(function(error) {
            this.logger.error(
                `Error while WHAT THE FUCK THIS PROMISES`
            );
            this.logger.error(error);
          }.bind(this));
      promises.push(promise);
    }
    return Promise.all(promises);
  }

  makeRemoteUrl(pageIndex) {
    const url = `https://www.homegate.ch/rent/real-estate/surrounding-zip-${this.POSTAL_AREA}/matching-list?ah=${this.RENT_LIMIT}&ac=3&ak=70&tab=list&o=sortToplisting-desc&be=${this.RADIUS}`;
    if (pageIndex) {
      return url + `&ep=${pageIndex}`;
    }
    return url;
  }

  extractListHomegateProperties(uri, data) {
    if (data === null || data === undefined) {
      this.logger.error('Error the html content is ' + data);
      return;
    }
    this.logger.info({
      service: this.getName(),
      msg: 'Document loaded from ' + uri,
      documentSize: data.length
    });
    const promises = [];
    const articles = this.getElementsInPage(data);
    if (articles) {
      for (let i = 0; i < articles.length; i++) {
        const article = articles[i];
        const promise = this.extractor.extract(article.href);
        if (promise) {
          promises.push(promise);
        } else {
          this.logger.error('Could not create the promise ' + article.href);
        }
      }
    } else {
      this.logger.error('Unable to retrieve the Elements in Page');
    }
    return Promise.all(promises).
        then(function(nxProperties) {
          this.logger.info('Loaded everything ' +
              nxProperties.length
          );
          for (let i = 0; i < nxProperties.length; i++) {
            this.redisCache.put(nxProperties[i].uri,
                JSON.stringify(nxProperties[i]));
          }
        }.bind(this)).
        catch(function(error) {
          this.logger.error(error);
        }.bind(this));
  }

  /**
   *
   * @param {string} data
   * @returns {*|NodeListOf<HTMLElementTagNameMap[string]>|NodeListOf<Element>|NodeListOf<SVGElementTagNameMap[string]>|undefined}
   */
  getElementsInPage(data) {
    const RESULT_LIST_CLASS = 'ResultListPage_stickyParent_3ocYo';
    const RESULT_ITEM_CLASS = 'ResultlistItem_itemLink_UKXbv';
    try {
      const document = new JSDOM(data).window.document;
      const list = document.querySelector('div.' + RESULT_LIST_CLASS);
      if (list) {
        const items = list.querySelectorAll('a.' + RESULT_ITEM_CLASS);
        if (items || items.length > 0) {
          return items;
        }
      }
      const subString = data.substr(0, 64);
      this.logger.error(
          `Cannot find the expected structure in the html document ${subString} ...`);
      if (data.indexOf(RESULT_LIST_CLASS) === -1) {
        this.logger.error(
            'the HTML Document does not contain the html marker for the List of Properties');
      }
      if (data.indexOf(RESULT_ITEM_CLASS) === -1) {
        this.logger.error(
            'the HTML Document does not contain the html marker for the Single Property');
      }
    } catch (exception) {
      this.logger.error(exception);
      return undefined;
    }
  }

  getKeySet(response, filterPredicate) {
    return this.redisCache.getKeySet(response, filterPredicate);
  }
}
