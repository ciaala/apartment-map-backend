import express from 'express';
import path from 'path';
import lightshipModule from 'lightship';
import Logger from '../util/Logger.js';
import DataFacade from '../data/DataFacade.js';
const __dirname = path.resolve();
export default class API {

  constructor({port}, services) {
    this.dataFacade = services.get('DataFacade');
    this.logger = Logger();
    this.port = port;

  }

  static getDependencies() {
    return [DataFacade];
  }

  handleData(request, response) {
    this.dataFacade.getRawData(response);
  }

  init() {
    return new Promise((resolve, fail) => {
      // Lightship will start a HTTP service on port 9000.
      this.lightship = lightshipModule.createLightship();
      this.lightship.registerShutdownHandler(
          function() {
            this.server.close();
            this.logger.final((logger) => logger.info('Shutdown now!'));
            this.logger.final((logger) => logger.info('Shutdown now!'));
          }
      );
      this.startWebApplication(this.port);
      resolve();
    });
  }

  /**
   *
   * @param port
   */
  startWebApplication(port) {
    const app = express();
    const publicBuild = path.join(__dirname, 'client', 'build');
    const staticIndex = express.static( publicBuild );

    app.use('/', function(request,response,next) {
      return staticIndex(request,response,next);
    });

    app.use('/data', this.handleData.bind(this));

    app.use('/info',
        function(request, response, next) {
          const userAgent = request.get('User-Agent');
          this.logger.info('Connection received for path / from ' + userAgent);
          response.send('up and running');
        }.bind(this)
    );

    return app.listen(port, function() {
      this.logger.info('Connection open');
      this.lightship.signalReady();
    }.bind(this));
  }
}
